#!/usr/bin/env python3
#
# moin2commonmark.py
# MoinMoin wiki to CommonMark converter
# Copyright 2020 Gerald Combs <gerald@wireshark.org>
#
# SPDX-License-Identifier: MIT
#

from bs4 import BeautifulSoup

import argparse
import configparser
import copy
import json
import logging
import lxml.html
import lxml.html.clean
import os
import os.path
import re
import stat
import subprocess
import sys
import time
import urllib.request, urllib.error, urllib.parse

def exit_err(message):
    logging.critical(message)
    sys.exit(1)

open_url_sleep_time = 0.1
def open_url(url, encoding=None):
    '''Open a URL.
    Returns a tuple containing the body and response dict. The body is a
    UTF-8 encoded str.
    '''
    req_headers = { 'User-Agent': 'Wireshark moin2commonmark' }
    try:
        req = urllib.request.Request(url, headers=req_headers)
        response = urllib.request.urlopen(req)
        if response.geturl() != url:
            time.sleep(open_url_sleep_time * 4)
            return (None, 302)
        if encoding:
            body = response.read().decode(encoding, 'replace')
        else:
            body = response.read()
    except urllib.error.HTTPError as err:
        exit_err('Error {} opening {}'.format(err.code, url))
    except Exception as err:
        exit_err('Error opening {}: {}'.format(url, err))

    time.sleep(open_url_sleep_time)
    return (body, response.getcode())

def get_title_index_list(moin_url):
    '''\
        Return a list of (moin_path, has_attachments) tuples, e.g.
        [('/AARP', False), ('/SampleCaptures', True)]
    '''
    page_url = moin_url + '/TitleIndex'
    page_list = [('/', True)] # TitleIndex doesn't include /
    (body, status) = open_url(page_url, encoding='UTF-8')
    if status != 200:
        exit_err('GETting {} returned {}.'.format(page_url, status))
    soup = BeautifulSoup(body, 'html.parser')
    for li in soup.select('div#content li'):
        # There's a "See also" section at the top inside a <p/>.
        # We set recursive=False in order to skip over them.
        anchors = li.find_all('a', recursive=False)
        if len(anchors) < 1: continue
        moin_path = anchors[0].get('href')
        has_attachments = len(anchors) > 1
        page_list += [(moin_path, has_attachments)]
    return page_list

def skip_page(moin_path):
    skip_prefixes = (
        '/AdministratorGroup',
        '/EditorGroup',
        '/HelpOnInstalling/',
        '/MoinMoin/',
        '/SiteNavigation',
        '/SyntaxReference',
        '/SystemPagesInEnglishGroup',
    )
    if moin_path.startswith(skip_prefixes):
        return True

    # Older versions of Moin had flat directory layouts and we ended up with a bunch
    # of pages with "_2f" directory separators.
    if '_2f' in moin_path:
        return True

    # Asn2wrs directives, e.g. #.EXPORTS
    if moin_path.startswith('/%23.'):
        return True

    return False

def gitlab_path(moin_path):
    gl_path = urllib.parse.unquote_plus(moin_path)
    # GitLab / Gollum replaces filename spaces with dashes
    return gl_path.replace(' ', '-')

moin_upload_dir = os.path.join('uploads', '__moin_import__')
def moin_attachment_path_to_gitlab(href):
    # Moin attachments are per-page, so key them as <page>/<target>.
    href_parts = urllib.parse.urlparse(href)
    href_path = href_parts.path.lstrip('/')
    target = urllib.parse.parse_qs(href_parts.query)['target'][0]
    att_path = os.path.join(moin_upload_dir, 'attachments', href_path, target)
    return gitlab_path(att_path)

class WikiPage:
    def __init__(self, moin_url, moin_path, import_dir):
        self.moin_path = moin_path
        self.cm_path = self.moin_page_path_to_gitlab(self.moin_path)
        self.page_url = moin_url + moin_path
        self.content = None
        # self.is_directory = False
        self.gmtime = time.gmtime()
        self.category_re = re.compile('/Category[A-Za-z]+')
        self.page_path = os.path.join(import_dir, self.cm_path.lstrip('/')) + '.md'
        self.page_exists = False

        mu_host = urllib.parse.urlparse(moin_url).netloc
        self.moin_url_re = re.compile('https?://' + re.escape(mu_host), re.IGNORECASE)

        try:
            statinfo = os.stat(self.page_path)
            if statinfo.st_size > 0 and stat.S_ISREG(statinfo.st_mode):
                self.page_exists = True
                return
        except FileNotFoundError:
            pass

        logging.info('Fetching ' + self.page_url)
        (body, status) = open_url(self.page_url, encoding='UTF-8')
        if status == 302:
            logging.info('GETting {} redirected.'.format(self.page_url))
            return None
        if status != 200:
            logging.warning('GETting {} returned {}.\n'.format(self.page_url, status))
            return None

        # Moin produces markup that trips up html.parser, e.g.
        # closed <img /> tags and extra </li>and </ol> tags.
        allowed_attrs = lxml.html.defs.safe_attrs | set({'style'})
        cleaner = lxml.html.clean.Cleaner(safe_attrs=allowed_attrs)
        body = cleaner.clean_html(body)

        soup = BeautifulSoup(body, 'html.parser')
        self.content = soup.select('div#content')
        self.cleanup_page_content()

    def moin_icon_to_cm_emoji(self, moin_href):
        icon_to_emoji = {
            # https://moinmo.in/HelpOnNavigation
            # https://www.webfx.com/tools/emoji-cheat-sheet/
            'alert.png': 'warning',
            'angry.png': 'angry',
            'attach.png': 'paperclip',
            'attention.png': 'heavy_exclamation_mark',
            'biggrin.png': 'grinning',
            'checkmark.png': 'white_check_mark',
            'devil.png': 'smiling_imp',
            'frown.png': 'frowning',
            'icon-error.png': 'x',
            'icon-info.png': 'information_source',
            'idea.png': 'bulb',
            'ohwell.png': 'confused',
            'prio1.png': 'one',
            'prio2.png': 'two',
            'prio3.png': 'three',
            'redface.png': 'blush',
            'sad.png': 'frowning',
            'smile.png': 'smiley',
            'smile2.png': 'sunglasses',
            'smile3.png': 'laughing',
            'smile4.png': 'wink',
            'star_off.png': 'star',
            'star_on.png': 'star2',
            'thumbs-up.png': 'thumbsup',
            'tired.png': 'tired_face',
            'tongue.png': 'stuck_out_tongue',
        }
        cm_emoji = None
        if moin_href.startswith('/moin_static') and moin_href.endswith('.png'):
            icon = moin_href.split('/')[-1]
            cm_emoji = ':' + icon_to_emoji[icon] + ':'
        return cm_emoji

    def moin_page_path_to_gitlab(self, moin_path):
        '''Convert a Moin URL path to a CommonMark file path'''
        if moin_path in ['/', '/FrontPage']:
            return 'home'
        return gitlab_path(moin_path)

    def is_empty(self, tag):
        img_count = len(tag.find_all('img'))
        if len(tag.get_text(strip=True)) == 0 and img_count == 0:
            return True
        return False

    def extract_up(self, tag):
        '''Extract a tag and its empty parents'''
        extract_empty_parents = ['li', 'p', 'span', 'ul']
        tag_parent = tag.parent
        tag.extract()
        while tag_parent.name in extract_empty_parents:
            if not self.is_empty(tag_parent):
                break
            cur_parent = tag_parent
            tag_parent = tag_parent.parent
            cur_parent.extract()

    def cleanup_page_content(self):
        for tag in self.content:
            # Remove empty anchor spans
            # <span id="top" class="anchor"></span>
            # <span id="line-2" class="anchor"></span>
            # <span id="bottom" class="anchor"></span>
            for span in tag.select('span.anchor#top, span.anchor[id^=line-], span.anchor#bottom'):
                if self.is_empty(span):
                    span.extract()

            # Replace Moin <<TableOfContents>> with GitLab's [[_TOC_]]
            # XXX Is there a way to keep Pandoc from replacing this with
            # '\[\[\_TOC\_\]\]'? We hack around this in upload_gl_page.
            for div in tag.select('div.table-of-contents'):
                div.insert_before('[[_TOC_]]')
                div.extract()

            # Remove empty <p class="line862">...</p> tags.
            # Remove the "line..." class from non-empty ones.
            for p in tag.select('p[class^=line]'):
                if self.is_empty(p):
                    p.extract()
                else:
                    del p.attrs['class']

            # De-indent MoinMoin indentation, which doesn't translate well.
            for li in tag.select('ul li[style="list-style-type:none"]'):
                if li.next_sibling or li.previous_sibling:
                    continue
                ul = li.parent
                for content_tag in li.contents:
                    ul.insert_before(copy.copy(content_tag))
                ul.extract()

    def replace_links(self):
        for tag in self.content:
            for link_tag in tag.find_all(['a', 'img']):
                link_attr = 'href'
                if link_tag.name == 'img':
                    link_attr = 'src'
                try:
                    moin_href = link_tag.attrs[link_attr]

                    # Make absolute Moin links relative
                    moin_href = self.moin_url_re.sub('', moin_href)

                    # Leave external links alone.
                    if '://' in moin_href:
                        continue

                    if self.category_re.match(moin_href):
                        self.extract_up(link_tag)
                        continue

                    cm_emoji = self.moin_icon_to_cm_emoji(moin_href)
                    if cm_emoji:
                        logging.info('Replacing img src=' + moin_href + ' with ' + cm_emoji)
                        link_tag.replace_with(cm_emoji)
                        continue

                    cm_href = self.moin_page_path_to_gitlab(moin_href)

                    if '?action=AttachFile' in moin_href:
                        cm_href = moin_attachment_path_to_gitlab(moin_href)

                    logging.info('{}: Rewriting {} to {}'.format(self.moin_path, moin_href, cm_href))
                    link_tag.attrs[link_attr] = cm_href

                except KeyError:
                    logging.warning('Found a or img without href: ' + repr(link_tag))

    def page_html(self):
        if not self.content:
            return None
        return '\n'.join(str(item) for item in self.content[0])

    def write_page(self):
        if not self.content:
            logging.info(self.cm_path + ' has no content. Skipping.')
            return

        if self.page_exists:
            logging.info('Page {} exists. Skipping.'.format(self.page_path))
            return

        # https://pandoc.org/MANUAL.html#markdown-variants
        pd_proc = subprocess.run([
                'pandoc',
                '--from=html+raw_html',
                '--to=commonmark',
                '--wrap=none',
            ],
            encoding='UTF-8',
            input=self.page_html(),
            capture_output=True
            )
        if pd_proc.returncode != 0:
            exit_err('pandoc returned {}'.format(pd_proc.returncode))
        cm_page = pd_proc.stdout

        # Fix up CommonMark conversion artifacts.
        cm_page = cm_page.replace('\[\[\_TOC\_\]\]', '[[_TOC_]]')

        # Check table conversions
        html_table_count = 0
        for tag in self.content:
            html_table_count += len(tag.find_all('table'))
        cm_table_count = cm_page.count('<table>')
        if html_table_count != cm_table_count:
            logging.warning('{} has {} input tables and {} output tables'.format(self.page_path, html_table_count, cm_table_count))

        cm_page += '\n'.join([
            '',
            '---',
            '',
            'Imported from {} on {}\n'.format(self.page_url, time.strftime('%Y-%m-%d %H:%M:%S UTC', self.gmtime))
        ])

        try:
            os.makedirs(os.path.dirname(self.page_path))
        except FileExistsError:
            pass

        try:
            with open(self.page_path, 'w') as page_f:
                page_f.write(cm_page)
            logging.info('Saved page {} ({} bytes)'.format(self.page_path, len(cm_page)))
        except Exception as err:
            exit_err('Failed to save page {}: {}'.format(self.page_path, err))

def main():
    this_dir = os.path.dirname(__file__)
    config = configparser.ConfigParser()
    config.read(os.path.join(this_dir, 'moin2commonmark.cfg'))

    moin_url = config['DEFAULT']['moin_url']

    parser = argparse.ArgumentParser(description='MoinMoin to CommonMark migration')
    parser.add_argument('--import-directory', default=config['DEFAULT']['import_directory'], help='Import directory')
    parser.add_argument('--log-file', default=None, help='Log file')
    parser.add_argument('--sleep-time', default=0.1, help='Seconds to sleep after each MoinMoin request')
    parser.add_argument('--start-page', default=None, help='Start processing beginning with /PageName')
    parser.add_argument('--stop-page', default=None, help='Stop processing after /PageName')
    parser.add_argument('--skip-attachments', action='store_true', help='Skip processing attachments')
    args = parser.parse_args()

    args.import_directory = os.path.normpath(args.import_directory)
    try:
        os.makedirs(os.path.join(args.import_directory, moin_upload_dir))
    except FileExistsError:
        pass

    if not args.log_file:
        args.log_file = os.path.join(args.import_directory, moin_upload_dir, 'moin_import.log')
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s %(levelname)s %(message)s',
        handlers=[
            logging.FileHandler(args.log_file),
            logging.StreamHandler(sys.stdout)
        ]
    )

    open_url_sleep_time = args.sleep_time

    in_start_stop = True
    if args.start_page:
        in_start_stop = False
        logging.info('Starting with ' + args.start_page)
    if args.stop_page:
        logging.info('Stopping after ' + args.stop_page)

    title_index_list = get_title_index_list(moin_url)
    directory_d = {}

    attachment_index_list = title_index_list
    if args.skip_attachments:
        logging.info('Skipping attachments')
        attachment_index_list = {}

    for (moin_path, has_attachments) in title_index_list:
        dirname = os.path.dirname(moin_path)
        if dirname != '/':
            directory_d[dirname] = True

        if not has_attachments or args.skip_attachments:
            continue

        if args.start_page and moin_path == args.start_page:
                in_start_stop = True

        if skip_page(moin_path) or not in_start_stop:
            logging.debug('Skipping attachments for ' + moin_path)
            continue

        if args.stop_page and moin_path == args.stop_page:
                in_start_stop = False

        logging.info('Getting attachments for ' + moin_path)
        att_list_url = moin_url + moin_path + '?action=AttachFile'
        (body, status) = open_url(att_list_url, encoding='UTF-8')
        if status != 200:
            exit_err('GETting {} returned {}.'.format(att_list_url, status))
        soup = BeautifulSoup(body, 'html.parser')

        for a in soup.select('div#content li a'):
            try:
                href = a.attrs['href']
                if '?action=AttachFile&do=get' in href:
                    att_path = os.path.normpath(os.path.join(args.import_directory, moin_attachment_path_to_gitlab(href)))
                    if not att_path.startswith(args.import_directory):
                        print('aid ' + args.import_directory + ' mat ' + moin_attachment_path_to_gitlab(href))
                        logging.warning('Attachment {} not within {}'.format(att_path, args.import_directory))
                        continue

                    try:
                        statinfo = os.stat(att_path)
                        if statinfo.st_size > 0 and stat.S_ISREG(statinfo.st_mode):
                            logging.info('Attachment {} exists. Skipping.'.format(att_path))
                            continue
                    except FileNotFoundError:
                        pass

                    moin_att_url = moin_url + href
                    (filedata, status) = open_url(moin_att_url)
                    if status != 200:
                        exit_err('GETting {} returned {}.'.format(moin_att_url, status))

                    try:
                        os.makedirs(os.path.dirname(att_path))
                    except FileExistsError:
                        pass

                    try:
                        with open(att_path, 'wb') as att_f:
                            att_f.write(filedata)
                        logging.info('Saved attachment {} ({} bytes)'.format(att_path, len(filedata)))
                    except Exception as err:
                        exit_err('Failed to save attachment {}: {}'.format(att_path, err))
            except KeyError:
                pass

    in_start_stop = True
    if args.start_page:
        in_start_stop = False

    for (moin_path, _) in title_index_list:
        if args.start_page and moin_path == args.start_page:
                in_start_stop = True

        if skip_page(moin_path) or not in_start_stop:
            logging.info('Skipping page ' + moin_path)
            continue

        if args.stop_page and moin_path == args.stop_page:
                in_start_stop = False

        page = WikiPage(moin_url, moin_path, args.import_directory)
        if not page.content:
            continue

        # page.is_directory = moin_path in directory_d
        page.replace_links()

        page.write_page()

if __name__ == '__main__':
    main()
