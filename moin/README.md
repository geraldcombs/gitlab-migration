# Moin to GitLab Wiki CommonMark Converter.

This directory contains a Python 3 script and assets that will migrate a [MoinMoin](https://moinmo.in) wiki to a GitLab wiki, including the following:

* README.md. This file.

* moin2commonmark. A convenience wrapper for moin2commonmark.py.
  It will create a Python 3 virtual environment (moin2commonmark.venv) as needed and run moin2commonmark.py in that virtual environment.

* moin2commonmark.cfg. Configuration settings.

* moin2commonmark.py. The wiki migration script.

## Getting Started

Prerequisites:

* Python 3
* Pandoc
* A MoinMoin wiki
* A GitLab wiki

Update moin/moin2commonmark.cfg as needed.

Check out the wiki into whatever *import_directory* points to.

Run `moin/moin2commonmark`.

Look for WARNING and ERROR messages in uploads/__moin_import__/moin_import.log

Commit everything in *import_directory* and and push it to the wiki.

## Caveats

MoinMoin doesn't provide an easy way to do page or attachment validation.
Want to clear the cache? Delete some files in the import directory.

Tables are rendered as HTML.
GitLab supports [pipe table markup](https://docs.gitlab.com/ee/user/markdown.html#tables), but that assumes your tables have headers.
Many of our tables [don't have headers](https://wiki.wireshark.org/Development/LibpcapFileFormat).
For the ones that do, Moin renders headers using \<td>\<strong> instead of \<th>.

## Top Pages

Below are the top 100 most-visited URLs for wiki.wireshark.org from January 1 to March 31, 2020.


[/DisplayFilters](https://wiki.wireshrk.org/DisplayFilters)

[/SampleCaptures](https://wiki.wireshrk.org/SampleCaptures)

[/CaptureFilters](https://wiki.wireshrk.org/CaptureFilters)

[/CaptureSetup/WLAN](https://wiki.wireshrk.org/CaptureSetup/WLAN)

[/](https://wiki.wireshrk.org/)

[/CaptureSetup](https://wiki.wireshrk.org/CaptureSetup)

[/WinPcap](https://wiki.wireshrk.org/WinPcap)

[/CaptureSetup/Ethernet](https://wiki.wireshrk.org/CaptureSetup/Ethernet)

[/TLS](https://wiki.wireshrk.org/TLS)

[/FrontPage](https://wiki.wireshrk.org/FrontPage)

[/CaptureSetup/USB](https://wiki.wireshrk.org/CaptureSetup/USB)

[/CaptureSetup/Loopback](https://wiki.wireshrk.org/CaptureSetup/Loopback)

[/CaptureSetup/CapturePrivileges](https://wiki.wireshrk.org/CaptureSetup/CapturePrivileges)

[/Tools](https://wiki.wireshrk.org/Tools)

[/HowToDecrypt802.11](https://wiki.wireshrk.org/HowToDecrypt802.11)

[/CaptureSetup/NetworkMedia](https://wiki.wireshrk.org/CaptureSetup/NetworkMedia)

[/LDAP](https://wiki.wireshrk.org/LDAP)

[/Lua](https://wiki.wireshrk.org/Lua)

[/TLS?action=show&redirect=SSL](https://wiki.wireshrk.org/TLS?action=show&redirect=SSL)

[/SNMP](https://wiki.wireshrk.org/SNMP)

[/Development/LibpcapFileFormat](https://wiki.wireshrk.org/Development/LibpcapFileFormat)

[/VoIP_calls](https://wiki.wireshrk.org/VoIP_calls)

[/DHCP](https://wiki.wireshrk.org/DHCP)

[/HelpContents](https://wiki.wireshrk.org/HelpContents)

[/CaptureSetup/VLAN](https://wiki.wireshrk.org/CaptureSetup/VLAN)

[/Gratuitous_ARP](https://wiki.wireshrk.org/Gratuitous_ARP)

[/VLAN](https://wiki.wireshrk.org/VLAN)

[/TCP ZeroWindow](https://wiki.wireshrk.org/TCP%20ZeroWindow)

[/SMB2](https://wiki.wireshrk.org/SMB2)

[/FindPage](https://wiki.wireshrk.org/FindPage)

[/RecentChanges](https://wiki.wireshrk.org/RecentChanges)

[/Lua/Examples](https://wiki.wireshrk.org/Lua/Examples)

[/SSDP](https://wiki.wireshrk.org/SSDP)

[/CaptureSetup/NetworkInterfaces](https://wiki.wireshrk.org/CaptureSetup/NetworkInterfaces)

[/WiresharkPortable](https://wiki.wireshrk.org/WiresharkPortable)

[/NetBIOS/NBNS](https://wiki.wireshrk.org/NetBIOS/NBNS)

[/Hyper_Text_Transfer_Protocol](https://wiki.wireshrk.org/Hyper_Text_Transfer_Protocol)

[/RTSP](https://wiki.wireshrk.org/RTSP)

[/BitTorrent](https://wiki.wireshrk.org/BitTorrent)

[/AddressResolutionProtocol](https://wiki.wireshrk.org/AddressResolutionProtocol)

[/DNS](https://wiki.wireshrk.org/DNS)

[/ProtocolReference](https://wiki.wireshrk.org/ProtocolReference)

[/Bluetooth](https://wiki.wireshrk.org/Bluetooth)

[/Ethernet](https://wiki.wireshrk.org/Ethernet)

[/NPcap](https://wiki.wireshrk.org/NPcap)

[/S7comm](https://wiki.wireshrk.org/S7comm)

[/DCE/RPC](https://wiki.wireshrk.org/DCE/RPC)

[/ColoringRules](https://wiki.wireshrk.org/ColoringRules)

[/CaptureSetup/Bluetooth](https://wiki.wireshrk.org/CaptureSetup/Bluetooth)

[/Development/PcapNg](https://wiki.wireshrk.org/Development/PcapNg)

[/CaptureSetup/Pipes](https://wiki.wireshrk.org/CaptureSetup/Pipes)

[/TCP_3_way_handshaking](https://wiki.wireshrk.org/TCP_3_way_handshaking)

[/SampleCaptures?action=AttachFile&do=view&target=003transf+(5).cap](https://wiki.wireshrk.org/SampleCaptures?action=AttachFile&do=view&target=003transf+(5).cap)

[/Protocols/ptp](https://wiki.wireshrk.org/Protocols/ptp)

[/WebSocket](https://wiki.wireshrk.org/WebSocket)

[/Wi-Fi](https://wiki.wireshrk.org/Wi-Fi)

[/RTP_statistics](https://wiki.wireshrk.org/RTP_statistics)

[/Internet_Control_Message_Protocol](https://wiki.wireshrk.org/Internet_Control_Message_Protocol)

[/Lua/Dissectors](https://wiki.wireshrk.org/Lua/Dissectors)

[/RTP](https://wiki.wireshrk.org/RTP)

[/LinkLayerDiscoveryProtocol](https://wiki.wireshrk.org/LinkLayerDiscoveryProtocol)

[/SSH](https://wiki.wireshrk.org/SSH)

[/TCP_Relative_Sequence_Numbers](https://wiki.wireshrk.org/TCP_Relative_Sequence_Numbers)

[/RDP](https://wiki.wireshrk.org/RDP)

[/IPv6](https://wiki.wireshrk.org/IPv6)

[/SIP](https://wiki.wireshrk.org/SIP)

[/HowToUseGeoIP](https://wiki.wireshrk.org/HowToUseGeoIP)

[/Security](https://wiki.wireshrk.org/Security)

[/Development/LifeCycle](https://wiki.wireshrk.org/Development/LifeCycle)

[/HTTP2](https://wiki.wireshrk.org/HTTP2)

[/CDP](https://wiki.wireshrk.org/CDP)

[/CategoryHowTo?action=show&redirect=HowTo](https://wiki.wireshrk.org/CategoryHowTo?action=show&redirect=HowTo)

[/SMTP](https://wiki.wireshrk.org/SMTP)

[/WakeOnLAN](https://wiki.wireshrk.org/WakeOnLAN)

[/APIPA](https://wiki.wireshrk.org/APIPA)

[/LuaAPI](https://wiki.wireshrk.org/LuaAPI)

[/DuplicatePackets](https://wiki.wireshrk.org/DuplicatePackets)

[/NetBIOS](https://wiki.wireshrk.org/NetBIOS)

[/GSoC2020](https://wiki.wireshrk.org/GSoC2020)

[/WireGuard](https://wiki.wireshrk.org/WireGuard)

[/CaptureSetup/Offloading](https://wiki.wireshrk.org/CaptureSetup/Offloading)

[/COTP](https://wiki.wireshrk.org/COTP)

[/Protocols/bacnet](https://wiki.wireshrk.org/Protocols/bacnet)

[/Protocols/isakmp](https://wiki.wireshrk.org/Protocols/isakmp)

[/Statistics](https://wiki.wireshrk.org/Statistics)

[/NTP](https://wiki.wireshrk.org/NTP)

[/Development/SubmittingPatches](https://wiki.wireshrk.org/Development/SubmittingPatches)

[/Protocols/frame](https://wiki.wireshrk.org/Protocols/frame)

[/LuaAPI/Tvb](https://wiki.wireshrk.org/LuaAPI/Tvb)

[/User_Datagram_Protocol](https://wiki.wireshrk.org/User_Datagram_Protocol)

[/Python](https://wiki.wireshrk.org/Python)

[/Tor](https://wiki.wireshrk.org/Tor)

[/STP](https://wiki.wireshrk.org/STP)

[/SMB](https://wiki.wireshrk.org/SMB)

[/ASTERIX](https://wiki.wireshrk.org/ASTERIX)

[/BuildingAndInstalling](https://wiki.wireshrk.org/BuildingAndInstalling)

[/libpcap](https://wiki.wireshrk.org/libpcap)

[/NetworkTroubleshooting](https://wiki.wireshrk.org/NetworkTroubleshooting)

[/TCP_Checksum_Verification](https://wiki.wireshrk.org/TCP_Checksum_Verification)

[/mpeg_dump.lua](https://wiki.wireshrk.org/mpeg_dump.lua)
