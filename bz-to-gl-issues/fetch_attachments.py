#!/usr/bin/env python3

import argparse
import base64
import datetime
import dateutil.parser
import hashlib
import json
import logging
import logging.handlers
import os
import os.path
import re
import requests
import sys
import time

# Globals

this_dir = os.path.dirname(__file__)
work_pfx = 'attachment-work.'
# https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file
# On macOS, colons are allowed in the Unix layer but not the Mac layer
# (e.g. the Finder).
invalid_chars_re = re.compile('[^A-Za-z0-9-_=+,.%~]')
underscore_re = re.compile('_+')
# Attachments 15252 and 15253 (bug 13417) return empty data.
max_empty = 100


def make_filename(attachment_dir, bug_id, att_id, raw_chars, prefix=''):
    sanitized_filename = invalid_chars_re.sub('_', raw_chars)
    sanitized_filename = underscore_re.sub('_', sanitized_filename)
    filename = f'{prefix}bug{bug_id}-attachment{att_id}{sanitized_filename}'
    return os.path.join(attachment_dir, filename)


def parse_attachment_ids(ids):
    bugids = set()
    for val in ids:
        try:
            start = int(val)
            stop = start
        except ValueError:
            start, stop = tuple(map(int, val.split('-')))
        bugids.update(range(start, stop+1))
    return sorted(bugids)


att_re = re.compile('bug[0-9]+-attachment([0-9]+)-')
def find_downloaded_attachment_ids(attachment_dir):
    att_ids = []
    for entry in os.scandir(attachment_dir):
        att_m = att_re.match(entry.name)
        if entry.is_file() and att_m:
            att_ids.append(int(att_m.group(1)))
        if entry.name.startswith(work_pfx):
            os.remove(entry.name)
            logging.warning('Removed leftover work file: {:s}'.format(entry.name))
    return att_ids

# JSON responses

# Valid, public attachment (#15808)
# Obsolete, public attachment (#6681)
#{
#  "attachments": {
#    "15808": {
#      "flags": [],
#      "content_type": "application/vnd.tcpdump.pcap",
#      "size": 923836,
#      "creator": "vendors_wireshark@wizwaretech.com",
#      "id": 15808,
#      "is_patch": 0,
#      "bug_id": 14010,
#      "file_name": "SIP_CALL_RTP_G711.pcap",
#      "last_change_time": "2017-08-27T19:09:25Z",
#      "is_obsolete": 0,
#      "is_private": 0,
#      "summary": "another sample pcap",
#      "creation_time": "2017-08-27T19:09:25Z"
#      "data": <Base64. Might be null, e.g. if is_obsolete = 1.>
#    }
#  },
#  "bugs": {}
#}

# Private attachment (#135 at the time of this writing)
#{
#  "code": 304,
#  "error": true,
#  "documentation": "https://bugzilla.readthedocs.org/en/5.0/api/",
#  "message": "Sorry, you are not authorized to access attachment #135."
#}

# Private bug (#141 at the time of this writing)
#{
#  "documentation": "https://bugzilla.readthedocs.org/en/5.0/api/",
#  "code": 102,
#  "message": "You are not authorized to access bug #541. To see this bug, you must first log in to an account with the appropriate permissions.",
#  "error": true
#}

# Nonexistent attachment (999999)
#{
#  "attachments": {},
#  "bugs": {}
#}

parser = argparse.ArgumentParser()
parser.add_argument('--debug', action='store_true',
                    help='Enable debug logging')
parser.add_argument('--bzurl', default='https://bugs.wireshark.org/bugzilla/',
                    help='Bugzilla base URL (default: %(default)s)')
parser.add_argument('--attachment-dir', default=os.path.join(this_dir, 'attachments'),
                    help='Directory in which to place attachments and metadata (default: <script_dir>/attachments)')
parser.add_argument('ids', nargs='+', default=[],
                    help='Attachment (not bug) ID or attachment ID ranges, e.g. 1 or 1-9')


def main():
    args = parser.parse_args()
    logging.basicConfig(format='%(asctime)s %(message)s',
                        level=logging.DEBUG if args.debug else logging.INFO)

    bzurl = args.bzurl
    attachment_dir = args.attachment_dir
    attachment_ids = parse_attachment_ids(args.ids)

    if not attachment_ids:
        logging.warning('No attachments selected')
        return
    logging.info('Selected %d attachments', len(attachment_ids))

    downloaded_attachment_ids = find_downloaded_attachment_ids(attachment_dir)
    ok_count = 0
    private_count = 0
    empty_count = 0

    starttime = time.monotonic()

    for att_id in attachment_ids:

        if att_id in downloaded_attachment_ids:
            continue

        try:
            url = f'{bzurl}/rest/bug/attachment/{att_id}'
            req = requests.get(url)
        except:
            logging.warning(f'Error fetching {url}. Bailing.')
            return(1)

        if 'error' in req.json() and 'code' in req.json():
            private_count += 1
            logging.info(f'Attachment {att_id} is private? Skipping.')
            continue

        if 'attachments' in req.json() and len(req.json()['attachments']) < 1:
            logging.info(f'Empty. Cur: {att_id}, count: {empty_count}.')
            empty_count += 1
            if empty_count <= max_empty:
                continue
            return(0)

        try:
            att_json = req.json()['attachments'][str(att_id)]
            if not att_json['data']:
                logging.info(f'No data for bug {att_json["bug_id"]}, attachment {att_id}. Obsolete? {att_json["is_obsolete"]}.')
                continue
            att_data = base64.b64decode(att_json['data'])
            if len(att_data) != att_json['size']:
                logging.warning(f'Size mismatch. Wanted {att_json["size"]}, got {len(att_data)}.')
                continue
            if not att_json['file_name']:
                logging.info(f'Empty filename for bug {att_json["bug_id"]}, attachment {att_id}. Obsolete? {att_json["is_obsolete"]}.')
                att_json['file_name'] = 'no-file-name-provided'
            bug_id = att_json['bug_id']
            att_name = '-' + att_json['file_name']
            file_name = make_filename(attachment_dir, bug_id, att_id, att_name)
            file_time = dateutil.parser.parse(att_json['creation_time']).timestamp()

            with open(file_name, 'wb') as af:
                af.write(att_data)
                af.close()
                os.utime(file_name, (file_time, file_time))
                att_hash = hashlib.sha256()
                att_hash.update(att_data)
                att_json['sha256'] = att_hash.hexdigest()
                logging.info(f'Wrote {file_name}. {len(att_data)} bytes, {att_hash.hexdigest()}.')
            json_name = make_filename(attachment_dir, bug_id, att_id, '.json')
            del att_json['data']
            with open(json_name, 'w') as jf:
                json.dump(att_json, jf, indent=2, sort_keys=True)
                jf.write('\n')
            ok_count += 1
            empty_count = 0

        except KeyError:
            logging.info('Failed at {:d}. Stopping.'.format(att_id))
            return(1)

    endtime = time.monotonic()
    logging.info(f'Stopped at {att_id}. {ok_count} downloaded, {private_count} private. Took {datetime.timedelta(seconds=endtime-starttime)}.')

if __name__ == '__main__':
    profile = 0
    if profile:
        profile.run('main()')
    else:
        sys.exit(main())

