#!/usr/bin/env python3
import argparse
import json
import logging
import os
import os.path
import re
import time

import requests


this_dir = os.path.dirname(__file__)
bug_dir = os.path.join(this_dir, 'bugs')


def fetch_user(session, bzurl, bug_users, name):
    r = session.get(f'{bzurl}/rest/user/{name}')
    obj = r.json()
    if r.status_code != 200:
        logging.warning('Failed to retrieve user %s: %s',
                        name, obj.get('message'))
        return

    real_name = obj['users'][0]['real_name']
    bug_users.update({name: {
        'real_name': real_name,
        'fetched': True
    }})
    logging.info(f'Fetched {real_name} for username {name}')


bug_re = re.compile(f'bug\d+.json')
bug_comment_re = re.compile(f'bug\d+-comment.json')
def update_bug_users(bug_users):
    '''Extract user information from existing bugs.'''
    for dirent in os.listdir(bug_dir):
        # Bugs have *_detail items, so grab them first.
        m = bug_re.match(dirent)
        if not m:
            continue
        try:
            with open(os.path.join(bug_dir, dirent), 'r') as bj_f:
                full_bug_attrs = json.load(bj_f)
        except:
            logging.warning(f'Unable to load {dirent}')
            continue

        # {
        #   "bugs": [
        #     {
        #       "alias": [],
        #       "assigned_to": "bugzilla-admin@wireshark.org",
        #       "assigned_to_detail": {
        #         "email": "bugzilla-admin@wireshark.org",
        #         "id": 1,
        #         "name": "bugzilla-admin@wireshark.org",
        #         "real_name": "Bugzilla Administrator"
        #       },
        try:
            for bug_attrs in full_bug_attrs['bugs']:
                for k, v in bug_attrs.items():
                    if k.endswith('_detail'):
                        if type(v) is list:
                            details = v
                        else:
                            details = [v]
                        for detail in details:
                            name = detail['name']
                            real_name = detail['real_name']
                            if (not name in bug_users or not bug_users[name]['fetched']):
                                bug_users.update({name: {
                                    'real_name': real_name,
                                    'fetched': True
                                }})
        except Exception as err:
            logging.warning(f'Unable to process {dirent}: {err}')
            continue

    for dirent in os.listdir(bug_dir):
        m = bug_comment_re.match(dirent)
        if not m:
            continue
        try:
            with open(os.path.join(bug_dir, dirent), 'r') as bc_f:
                full_bug_comments = json.load(bc_f)
        except:
            logging.warning(f'Unable to load {dirent}')
            continue

        #   "bugs": {
        #     "8": {
        #       "comments": [
        #         {
        #           "attachment_id": null,
        #           "bug_id": 8,
        #           "count": 0,
        #           "creation_time": "2005-04-10T18:24:26Z",
        #           "creator": "gerald@wireshark.org",
        try:
            for bug_id in full_bug_comments['bugs'].keys():
                for comment in full_bug_comments['bugs'][f'{bug_id}']['comments']:
                    name = comment['creator']
                    if not name in bug_users:
                        bug_users.update({name: {
                            'real_name': '',
                            'fetched': False
                        }})
        except Exception as err:
            logging.warning(f'Unable to process {dirent}: {err}')
            continue


parser = argparse.ArgumentParser()
parser.add_argument('--debug', action='store_true',
                    help='Enable debug logging')
parser.add_argument('--bzurl', default='https://bugs.wireshark.org/bugzilla/',
                    help='Bugzilla base URL (default: %(default)s)')
parser.add_argument('--bug-dir', default=os.path.join(this_dir, 'bugs'),
                    help='Directory containing the output of fetch_bugs.py (default: <script_dir>/bugs)')


def main():
    args = parser.parse_args()
    logging.basicConfig(format='%(asctime)s %(message)s',
                        level=logging.DEBUG if args.debug else logging.INFO)

    bzurl = args.bzurl
    bug_dir = args.bug_dir
    bug_users = {} # { 'email@addr.ess': { real_name': 'A. Name', 'fetched': True }

    try:
        with open(os.path.join(bug_dir, 'bug-users.json'), 'r') as bu_f:
            bug_users = json.load(bu_f)
            logging.info(f'Loaded {len(bug_users)} users.')
    except FileNotFoundError:
            logging.info(f'bug-users.json not found. Creating.')


    update_bug_users(bug_users)

    session = requests.Session()
    for name in bug_users:
        if not bug_users[name]['fetched']:
            fetch_user(session, bzurl, bug_users, name)

    with open(os.path.join(bug_dir, 'bug-users.json'), 'w') as bu_f:
        json.dump(bug_users, bu_f, indent=2, sort_keys=True)
        logging.info(f'Saved {len(bug_users)} users.')


if __name__ == '__main__':
    main()
