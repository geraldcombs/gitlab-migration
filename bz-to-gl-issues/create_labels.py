#!/usr/bin/env python3
import argparse
import gitlab
import json
import logging
import os
import os.path

this_dir = os.path.dirname(__file__)

# https://docs.gitlab.com/ce/api/labels.html

parser = argparse.ArgumentParser()
parser.add_argument('--debug', action='store_true',
                    help='Enable debug logging')
parser.add_argument('--pgsection', default='gitlab',
                    help='~/.python-gitlab.cfg section (default: %(default)s)')
parser.add_argument('project_path',
                    help='Project path, e.g. wireshark/wireshark')


def main():
    args = parser.parse_args()
    logging.basicConfig(format='%(asctime)s %(message)s',
                        level=logging.DEBUG if args.debug else logging.INFO)

    with open(os.path.join(this_dir, 'labels.json'), 'r') as lj_f:
        labels = json.load(lj_f)

    gl = gitlab.Gitlab.from_config(args.pgsection)
    gl.auth()

    gl_project = gl.projects.get(args.project_path)
    cur_labels = gl_project.labels.list(all=True)

    gl_label_keys = ['name', 'color', 'description']

    for full_label in labels:
        new_label = True
        label = { k: full_label[k] for k in gl_label_keys }
        for cl in cur_labels:
            if cl.name == label['name']:
                new_label = False
                cl.color = label['color']
                cl.description = label['description']
                cl.save()
                logging.info(f'Updated label {cl.name} id {cl.id}')
        if new_label:
            res = gl_project.labels.create(label)
            logging.info(f'Created label {label["name"]} {res.id}')

if __name__ == '__main__':
    main()
